import { createSelector } from 'reselect';
import isAfter from 'date-fns/is_after';
import { currentGame } from './game';

const isGameId = k => `${k}`[0] !== '_';

const savings = state => state.savings;

// {id}
const saveById = createSelector(
  (_, { id }) => id,
  savings,
  (id, saves) => saves[id]
);

export const getSavedGames = createSelector(
  savings,
  (games = {}) =>
    Object.values(games)
      .filter(entry => entry && !!entry.game)
      .sort((a, b) => {
        return a.savedAt > b.savedAt ? -1 : 1;
      })
);

export const canBeSavedCurrentGame = createSelector(
  state => state,
  currentGame,
  (state, game) => {
    const { id } = game;
    const savedGame = saveById(state, { id });
    return !savedGame || savedGame.savedAt < game.updatedAt;
  }
);

export const existSavedGames = createSelector(
  savings,
  savings => Object.keys(savings).filter(isGameId).length > 0
);

// {id}
export const getSavedGame = createSelector(
  (_, { id }) => id,
  saveById,
  (id, save) => save && save.game
);

// {id}
export const getLatestSavedGame = createSelector(
  state => state,
  savings,
  (state, savings) => {
    const latest = Object.keys(savings).reduce((last, key) => {
      return !last || isAfter(savings[key].savedAt, savings[last].savedAt)
        ? key
        : last;
    });
    return latest && getSavedGame(state, { id: latest });
  }
);
