import { createSelector } from 'reselect';

const getGame = state => state.game.present;

export const currentGame = getGame;

export const hasPastHistory = state =>
  state.game.past && state.game.past.length > 0;

export const hasFutureHistory = state =>
  state.game.future && state.game.future.length > 0;

export const getCurrentGameBoardSize = createSelector(
  getGame,
  ({ boardSize }) => boardSize
);

export const getCurrentGameDifficulty = createSelector(
  getGame,
  ({ difficulty }) => difficulty
);

export const getCells = createSelector(
  getGame,
  ({ cells }) => cells
);

// {index}
export const getCell = createSelector(
  (_, { index }) => index,
  getGame,
  (index, game) => game.cells[index]
);

export const getCurrentCell = createSelector(
  getGame,
  game => game.cells[game.currentCell]
);

export const isCurrentCellEditable = createSelector(
  getCurrentCell,
  cell => !!cell.editable
);

// {index}
export const cellValue = createSelector(
  getCell,
  cell => cell.value
);

// {index}
export const cellGuesses = createSelector(
  getCell,
  cell => cell.guesses || []
);

// {index}
export const cellIsGuessing = createSelector(
  cellGuesses,
  guesses => guesses.length > 0
);

// {index}
export const cellIsEditable = createSelector(
  getCell,
  cell => !!cell.editable
);

// {index}
export const cellIsSelected = createSelector(
  (_, { index }) => index,
  getGame,
  (index, game) => game.currentCell === index
);

// {index}
export const cellHasError = createSelector(
  getCell,
  ({ error }) => !!error
);

// {index}
export const cellIsPositionallyRelated = createSelector(
  getCell,
  getGame,
  ({ col, row }, { currentColumn, currentRow, boardSize: size }) => {
    return col === currentColumn || row === currentRow;
  }
);

// {index}
export const cellIsNumericallyRelated = createSelector(
  getCell,
  getGame,
  ({ value }, { currentCell, cells }) => {
    return value !== null && value === cells[currentCell].value;
  }
);

export const getCurrentGameId = createSelector(
  getGame,
  (game = {}) => game.id || null
);

export const isBoardFilled = createSelector(
  getGame,
  ({ filledCells, boardSize }) => filledCells === boardSize * boardSize
);

export const hasWin = createSelector(
  getGame,
  ({ finishedAt }) => finishedAt !== null
);

export const elapsed = createSelector(
  getGame,
  ({ startedAt = new Date(), finishedAt = new Date() }) =>
    finishedAt - startedAt
);

export const currentGameHasOneUserMove = createSelector(
  currentGame,
  ({ cells }) =>
    Array.isArray(cells) &&
    cells.findIndex(({ editable, value }) => editable && value) !== -1
);

export const isCurrentGameEditable = createSelector(
  currentGame,
  ({ editable }) => !!editable
);

/**
 * Tell if a custom sudoku is valid
 */
export const gameHasValidClues = createSelector(
  getCells,
  cells => cells.filter(({ value }) => !!value).length >= 17
);

/**
 * Search in the history the last index of the previous game
 */
export const getPreviousGameHistoryIndex = createSelector(
  state => state.game.past,
  getCurrentGameId,
  (past = [], currentId) => {
    const index = past.findIndex(({ id }) => id === currentId);
    return index > 0 ? index - 1 : past.length - 1;
  }
);
