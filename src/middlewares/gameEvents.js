import { ActionCreators } from 'redux-undo';
import { isBoardFilled, getCells, hasWin } from '../selectors/game';
import { isSolved } from '../utils';

const REDO_TYPE = ActionCreators.redo().type;

const checkWin = (store, next, action) => {
  next(action);
  const state = store.getState();
  if (isBoardFilled(state)) {
    if (!hasWin(state) && isSolved(getCells(state))) {
      store.dispatch({
        type: 'WIN',
      });
    }
  }
};

export default store => next => action => {
  switch (action.type) {
    case 'SET_CURRENT_CELL_VALUE':
    case REDO_TYPE:
      return checkWin(store, next, action);
    default:
      return next(action);
  }
};
