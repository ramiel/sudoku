import { currentGameHasOneUserMove } from '../selectors/game';
import { canBeSavedCurrentGame } from '../selectors/savings';

const CHANGE_GAME_ACTIONS = ['CREATE_GAME', 'LOAD_GAME'];

const onGameChange = store => next => action => {
  if (CHANGE_GAME_ACTIONS.indexOf(action.type) === -1) {
    next(action);
    return;
  }
  const state = store.getState();
  const canBeSaved = canBeSavedCurrentGame(state);
  const existOneMove = currentGameHasOneUserMove(state);
  if (!existOneMove || !canBeSaved) {
    next(action);
    return;
  }
  const confirmed = window.confirm(
    'The current game has not been saved. If you continue you loose all your progress: are you sure you want to continue?'
  );
  if (confirmed) {
    next(action);
  }
};

export default onGameChange;
