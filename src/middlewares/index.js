import gameEvents from './gameEvents';
import onSave from './onSave';
import onGameChange from './onGameChange';

export default [gameEvents, onSave, onGameChange];
