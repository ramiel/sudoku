import { persistor } from '../store';

export default store => next => action => {
  switch (action.type) {
    case 'SAVE_GAME':
      const res = next(action);
      persistor.flush();
      return res;
    default:
      return next(action);
  }
};
