import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore } from 'redux-persist';
import LogRocket from 'logrocket';
import reducers from './reducers';
import middlewares from './middlewares';

const rootReducer = combineReducers(reducers);

export const store = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(thunk, ...middlewares, LogRocket.reduxMiddleware())
    // other store enhancers if any
  )
);
export const persistor = persistStore(store);

window.store = store;
export default store;
