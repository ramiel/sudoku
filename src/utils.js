import uuidv4 from 'uuid/v4';
import pako from 'pako';
import einar from './einar';

const COLS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];

const einarCoordinatesToPositionBySize = size => coord => {
  const col = COLS.indexOf(coord.slice(0, 1));
  const row = coord.slice(1) * 1 - 1;
  const position = size * col + row;
  return position;
};

const indexToColumn = ({ size, index }) => index % size;

const indexToRow = ({ size, index }) => Math.floor(index / size);

/**
 * Given an index get its coorinates
 * @param {Number} size Board size (side of the side)
 */
const getCoordinatesFromIndexAndSize = size => {
  const blockSize = Math.sqrt(size);
  return index => {
    const col = indexToColumn({ size, index });
    const row = indexToRow({ size, index });

    const Rg = Math.floor(row / blockSize);
    const Cg = Math.floor(col / blockSize);
    const block = Rg * blockSize + Cg;
    return {
      index,
      col,
      row,
      block,
    };
  };
};

const emptyCell = {
  editable: true,
  value: null,
  error: false,
  guesses: [],
};

export const cellCountToDifficulty = count => {
  if (count < 28) return 'hard';
  if (count < 35) return 'medium';
  return 'easy';
};

/**
 * Return the related difficulty
 * @param {String} difficulty The selected difficulty
 */
const difficultyToCellCount = difficulty => {
  switch (difficulty) {
    case 'hard':
      return 20;
    case 'medium':
      return 28;
    case 'easy':
    default:
      return 35;
  }
};

/**
 * Generate a new set of cells
 * @param {Object} options
 */
export const generateCells = ({ size = 9, difficulty = 'easy' } = {}) => {
  const getCoordinatesFromIndex = getCoordinatesFromIndexAndSize(size);
  const einarCoordinatesToPosition = einarCoordinatesToPositionBySize(size);
  const einarCells = einar.generate(difficultyToCellCount(difficulty));
  const cells = [];
  const m = Object.entries(einarCells).reduce((acc, [key, value]) => {
    return {
      ...acc,
      [einarCoordinatesToPosition(key)]: value * 1,
    };
  }, {});
  for (let i = 0; i < size * size; i++) {
    cells.push(
      m[i]
        ? {
            ...emptyCell,
            editable: false,
            value: m[i],
            ...getCoordinatesFromIndex(i),
          }
        : {
            ...emptyCell,
            ...getCoordinatesFromIndex(i),
          }
    );
  }
  return cells;
};

/**
 * Generate a new set of empty cells. Useful to create an empty sudoku for the builder
 * @param {Object} options
 */
export const generateEmptyCells = ({ size = 9 } = {}) => {
  const cells = [];
  const getCoordinatesFromIndex = getCoordinatesFromIndexAndSize(size);

  for (let i = 0; i < size * size; i++) {
    cells.push({
      ...emptyCell,
      ...getCoordinatesFromIndex(i),
    });
  }
  return cells;
};

export const isSolved = cells => {
  return cells.findIndex(({ error }) => error === true) === -1;
};

export const curry = (fn, ...args) =>
  args.length >= fn.length ? fn(...args) : curry.bind(null, fn, ...args);

const hasRowError = (cells, cell) => {
  if (!cell.value) return false;
  return (
    cells.findIndex(({ row, index, value }) => {
      return index !== cell.index && row === cell.row && value === cell.value;
    }) !== -1
  );
};

const hasColError = (cells, cell) => {
  if (!cell.value) return false;
  return (
    cells.findIndex(({ col, index, value }) => {
      return index !== cell.index && col === cell.col && value === cell.value;
    }) !== -1
  );
};

const hasBlockError = (cells, cell) => {
  if (!cell.value) return false;
  return (
    cells.findIndex(({ block, index, value }) => {
      return (
        index !== cell.index && block === cell.block && value === cell.value
      );
    }) !== -1
  );
};

export const hasCellError = curry((cells, cell) => {
  return (
    hasRowError(cells, cell) ||
    hasColError(cells, cell) ||
    hasBlockError(cells, cell)
  );
});

export const serialize = cells => {
  var serialized = '';
  for (let i = 0, len = cells.length; i < len; i++) {
    serialized += !cells[i].editable ? cells[i].value : 'x';
  }

  serialized = serialized
    .replace(/xxxxxx/g, 'f')
    .replace(/xxxxx/g, 'e')
    .replace(/xxxx/g, 'd')
    .replace(/xxx/g, 'c')
    .replace(/xx/g, 'b')
    .replace(/x/g, 'a');
  return serialized;
};

export const deserialize = serialized => {
  const cells = [];
  let initialCellCount = 0;
  serialized = serialized
    .replace(/f/g, 'xxxxxx')
    .replace(/e/g, 'xxxxx')
    .replace(/d/g, 'xxxx')
    .replace(/c/g, 'xxx')
    .replace(/b/g, 'xx')
    .replace(/a/g, 'x');

  for (let i = 0, len = serialized.length; i < len; i++) {
    if (serialized.charAt(i) === 'x') {
      cells.push({ ...emptyCell });
    } else {
      initialCellCount += 1;
      cells.push({
        ...emptyCell,
        editable: false,
        value: serialized.charAt(i),
      });
    }
  }
  return { cells, difficulty: cellCountToDifficulty(initialCellCount) };
};

export const serializeSavedGame = game => {
  return window.btoa(pako.deflate(JSON.stringify(game), { to: 'string' }));
};

export const deserializeSavedGame = game => {
  return JSON.parse(pako.inflate(window.atob(game), { to: 'string' }));
};

export const createId = uuidv4;

export const percentageOf = (total, partial) =>
  total <= 0 ? 0 : (partial * 100) / total;
