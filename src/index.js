import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import './index.css';
import SudokuChooser from './components/SudokuChooser';
import ErrorBoundary from './components/ErrorBoundary';
import { store, persistor } from './store';
import * as serviceWorker from './serviceWorker';
import { Provider as ReakitProvider } from 'reakit';
import theme from 'reakit-theme-default';
import * as Sentry from '@sentry/browser';
import LogRocket from 'logrocket';
import { Analytics } from '@vercel/analytics/react';

LogRocket.init('7qa8yc/sudoku');
Sentry.init({
  dsn: 'https://ad1d74bea2654ce6913efb201e4cc929@sentry.io/1429353',
  environment: process.env.NODE_ENV || 'development',
});
LogRocket.getSessionURL(sessionURL => {
  Sentry.configureScope(scope => {
    scope.setExtra('sessionURL', sessionURL);
  });
});

ReactDOM.render(
  <Provider store={store}>
    <ReakitProvider theme={theme}>
      <ErrorBoundary>
        <PersistGate persistor={persistor}>
          <SudokuChooser />
        </PersistGate>
      </ErrorBoundary>
    </ReakitProvider>
    <Analytics />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
