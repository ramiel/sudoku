import { connect } from 'react-redux';
import {
  moveCellUp,
  moveCellDown,
  moveCellRight,
  moveCellLeft,
  clearCurrentCell,
  setCurrentCellValue,
  toggleCurrentCellGuess,
} from '../../actions/game';
import {
  getCurrentGameBoardSize,
  isCurrentCellEditable,
  isCurrentGameEditable,
} from '../../selectors/game';
import SudokuTable from './SudokuTable';

const mapDispatchToProps = {
  moveCellUp,
  moveCellDown,
  moveCellRight,
  moveCellLeft,
  clearCurrentCell,
  setCurrentCellValue,
  toggleCurrentCellGuess,
};

const mapStateToProps = (state, props) => ({
  size: getCurrentGameBoardSize(state),
  currentCellEditable: isCurrentCellEditable(state),
  disableGuess: isCurrentGameEditable(state),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SudokuTable);
