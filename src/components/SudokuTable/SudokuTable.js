import React, { useEffect } from 'react';
import { Grid } from 'reakit';
import CellsBlock from './CellsBlock';
import './SudokuTable.scss';

const DIRECTION_KEYS = ['ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'];
const VALID_NUMBERS = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
const DELETE_KEYS = ['Delete', 'Backspace'];

const onKeyDown = ({
  moveCellUp,
  moveCellDown,
  moveCellRight,
  moveCellLeft,
}) => {
  return e => {
    switch (e.key) {
      case DIRECTION_KEYS[0]:
        e.preventDefault();
        return moveCellUp();
      case DIRECTION_KEYS[1]:
        e.preventDefault();
        return moveCellDown();
      case DIRECTION_KEYS[2]:
        e.preventDefault();
        return moveCellLeft();
      case DIRECTION_KEYS[3]:
        e.preventDefault();
        return moveCellRight();
      default:
        return;
    }
  };
};

const onKeyUp = ({
  currentCellEditable,
  clearCurrentCell,
  setCurrentCellValue,
  toggleCurrentCellGuess,
  disableGuess,
}) => {
  return e => {
    if (currentCellEditable) {
      if (DELETE_KEYS.indexOf(e.key) !== -1) {
        return clearCurrentCell();
      } else if (VALID_NUMBERS.indexOf(e.key) !== -1) {
        return e.metaKey && !disableGuess
          ? toggleCurrentCellGuess(e.key * 1)
          : setCurrentCellValue(e.key * 1);
      }
    }
  };
};

const SudokuTable = ({
  // from container
  size,
  moveCellUp,
  moveCellDown,
  moveCellRight,
  moveCellLeft,
  currentCellEditable,
  clearCurrentCell,
  setCurrentCellValue,
  toggleCurrentCellGuess,
  disableGuess,
  className = '',
}) => {
  const sideSize = Math.sqrt(size);
  const content = [];

  // Hooks
  useEffect(() => {
    const fnKeyDown = onKeyDown({
      moveCellUp,
      moveCellDown,
      moveCellRight,
      moveCellLeft,
    });
    const fnKeyUp = onKeyUp({
      currentCellEditable,
      clearCurrentCell,
      setCurrentCellValue,
      toggleCurrentCellGuess,
      disableGuess,
    });
    window.addEventListener('keydown', fnKeyDown);
    window.addEventListener('keyup', fnKeyUp);
    return () => {
      window.removeEventListener('keydown', fnKeyDown);
      window.removeEventListener('keyup', fnKeyUp);
    };
  });

  for (let i = 0; i < size; i++) {
    content.push(<CellsBlock size={sideSize} index={i} key={`block-${i}`} />);
  }

  return (
    <Grid
      className={`sudoku-grid ${className}`}
      templateColumns={`repeat(${sideSize}, 1fr)`}
      templateRows={`repeat(${sideSize}, 1fr)`}
      gap="0"
      role="rowgroup"
    >
      {content}
    </Grid>
  );
};

export default SudokuTable;
