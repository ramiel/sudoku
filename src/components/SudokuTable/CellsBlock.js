import React from 'react';
import { Grid } from 'reakit';
import Cell from '../Sudoku/Cell';

const CellsBlock = ({ size, index, CellComponent = Cell }) => {
  const content = [];
  const cellFactor = (index % size) * size;
  const rowFactor = Math.floor(index / size) * size;
  const gridSize = size * size;
  for (let i = 0; i < size; i++) {
    for (let j = 0; j < size; j++) {
      const rowG = rowFactor + i;
      const colG = cellFactor + j;
      const cellIndex = rowG * gridSize + colG;
      content.push(<CellComponent index={cellIndex} key={cellIndex} />);
    }
  }

  return (
    <Grid
      templateColumns={`repeat(${size}, 1fr)`}
      templateRows={`repeat(${size}, 1fr)`}
      gap="0"
      placeItems="center"
      border="1px solid black"
      role="row"
    >
      {content}
    </Grid>
  );
};

export default CellsBlock;
