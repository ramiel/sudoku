import React, { useState, memo } from 'react';
import Button from '../Button';
import { Flex, Popover } from 'reakit';
import './GameResumeModal.scss';

const TIMEOUT_SECS = 5;

const GameResumeModal = ({ loadLastSavedGame, existSavedGames, children }) => {
  const [visible, setVisible] = useState(existSavedGames);
  const [remaining, setRemaining] = useState(TIMEOUT_SECS);

  if (visible) {
    if (remaining > 0) {
      setTimeout(() => {
        setRemaining(remaining - 1);
      }, 1000);
    }
    setTimeout(() => {
      setVisible(false);
    }, TIMEOUT_SECS * 1000);
  }
  return (
    <>
      {children}
      <Popover
        className="game-resume-modal"
        visible={visible}
        placement="top-start"
        hideOnEsc
        hideOnClickOutside
        fade
      >
        <Popover.Arrow />
        <Flex column>
          <span>
            You have saved games,{' '}
            <Button
              onClick={() => {
                setVisible(false);
                loadLastSavedGame();
              }}
            >
              resume your last game
            </Button>
          </span>

          <small>
            <Button onClick={() => setVisible(false)}>Hiding</Button> in{' '}
            {remaining} secs...
          </small>
        </Flex>
      </Popover>
    </>
  );
};

export default memo(GameResumeModal);
