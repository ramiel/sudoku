import { connect } from 'react-redux';
import { loadLastSavedGame } from '../../actions/savings';
import { existSavedGames } from '../../selectors/savings';
import GameResumeModal from './GameResumeModal';

const mapDispatchToProps = {
  loadLastSavedGame,
};

const mapStateToProps = state => ({
  existSavedGames: existSavedGames(state),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GameResumeModal);
