import React, { PureComponent } from 'react';
import { Button, Flex } from 'reakit';
import * as Sentry from '@sentry/browser';

export default class ErrorBoundary extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { error: null };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error });
    Sentry.withScope(scope => {
      Object.keys(errorInfo).forEach(key => {
        scope.setExtra(key, errorInfo[key]);
      });
      Sentry.captureException(error);
    });
  }

  render() {
    if (this.state.error) {
      //render fallback UI
      return (
        <Flex column alignItems="center">
          <h2>Oh no! :(</h2>
          <p>An error occurred, we're sorry.</p>
          <p>
            We already sent any error detail to our servers but you can click on
            the button below to specify further information.
          </p>
          <Button onClick={() => Sentry.showReportDialog()}>
            Report feedback
          </Button>
        </Flex>
      );
    } else {
      //when there's not an error, render children untouched
      return this.props.children;
    }
  }
}
