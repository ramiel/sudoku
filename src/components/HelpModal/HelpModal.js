import React from 'react';
import { Block, Button, Backdrop, Portal, Overlay, Grid } from 'reakit';
import Icon from '../Icon';
import './HelpModal.scss';

export default ({ trigger, ...otherProps }) => {
  return (
    <Overlay.Container>
      {overlay => (
        <Block>
          {trigger ? (
            trigger(overlay)
          ) : (
            <Button use={Overlay.Show} {...overlay} {...otherProps}>
              Show help
            </Button>
          )}
          {overlay.visible ? (
            <>
              <Backdrop use={[Portal, Overlay.Hide]} {...overlay} />
              <Overlay
                use={Portal}
                {...overlay}
                className="help-modal-container"
              >
                <Block className="content">
                  <Button className="close-button" onClick={overlay.hide}>
                    <Icon icon="times" />
                  </Button>
                  <h2>Keyboard shortcuts</h2>
                  <Grid
                    templateColumns="repeat(2, 1fr)"
                    autoRows="auto"
                    gap="0.2rem 2vw"
                  >
                    <Grid.Item>
                      <strong>
                        <em>Key</em>
                      </strong>
                    </Grid.Item>
                    <Grid.Item>
                      <strong>
                        <em>Action</em>
                      </strong>
                    </Grid.Item>
                    <Grid.Item>
                      <strong>1..9</strong>
                    </Grid.Item>
                    <Grid.Item>Insert a number</Grid.Item>
                    <Grid.Item>
                      <strong>⌘ + 1..9</strong>
                      {' or '}
                      <strong>
                        <Icon icon={['fab', 'windows']} /> + 1..9
                      </strong>
                    </Grid.Item>
                    <Grid.Item>Insert a guess</Grid.Item>
                    <Grid.Item>
                      <strong>Arrows keys</strong>
                    </Grid.Item>
                    <Grid.Item>Select a cell</Grid.Item>
                    <Grid.Item>
                      <strong>
                        <Icon icon="backspace" />
                      </strong>
                      {' or '}
                      <strong>Del</strong>
                    </Grid.Item>
                    <Grid.Item>Remove the cell content</Grid.Item>
                    <Grid.Item>
                      <strong>Ctrl + z</strong>
                    </Grid.Item>
                    <Grid.Item>Undo last action</Grid.Item>
                    <Grid.Item>
                      <strong>Ctrl + Shift + z</strong> or{' '}
                      <strong>Ctrl + y</strong>
                    </Grid.Item>
                    <Grid.Item>Redo last action</Grid.Item>
                  </Grid>
                  <h2>On screen controls</h2>
                  <div className="long-help">
                    <p>
                      Click on the cell you want to modify, use the numeric pad
                      to insert a value in a cell. Click on{' '}
                      <Icon icon="backspace" /> button to delete the content of
                      a cell.
                    </p>
                    <p>
                      Use the <strong>Guess ON/OFF</strong> button to switch
                      between normal and guess mode.
                    </p>
                    <p>
                      Use the <Icon icon="undo" /> and <Icon icon="redo" />{' '}
                      buttons to undo/redo your moves.
                    </p>
                    <p>
                      To create a new game click on the <strong>New</strong>{' '}
                      button and choose the difficulty. If you choose{' '}
                      <strong>custom</strong> you can create your own board:
                      useful to copy a sudoku from a magazine!
                    </p>
                    <p>
                      Use the <strong>Save</strong> button to save a game. You
                      can continue your match clicking on the{' '}
                      <strong>Load</strong> at any moment. Your saved games are
                      persisted even if you close your browser or your device.
                    </p>
                    <p>
                      You can <strong>Share</strong> your games with your
                      friends; when you share a game you can decide to share
                      just the initial board or to include any move you've
                      already done.
                    </p>
                  </div>
                  <h2>Getting help</h2>
                  <p>
                    Need help? Something isn't working properly? You have a
                    suggestion?
                    <br />
                    Send us an{' '}
                    <a
                      href="mailto:incoming+ramiel-sudoku-11118659-issue-@incoming.gitlab.com"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      email
                    </a>
                    .
                  </p>
                  <small>Version {process.env.REACT_APP_VERSION}</small>
                </Block>
              </Overlay>
            </>
          ) : null}
        </Block>
      )}
    </Overlay.Container>
  );
};
