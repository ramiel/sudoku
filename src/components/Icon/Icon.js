import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faBuromobelexperte,
  faGitlab,
  faWindows,
} from '@fortawesome/free-brands-svg-icons';
import {
  faBackspace,
  faCalendarDay,
  faClipboard,
  faClock,
  faCloudDownloadAlt,
  faDove,
  faEgg,
  faKiwiBird,
  faFeatherAlt,
  faPencilAlt,
  faPercentage,
  faQuestionCircle,
  faRedo,
  faSave,
  faShareAlt,
  faTh,
  faTimes,
  faTrashAlt,
  faUndo,
} from '@fortawesome/free-solid-svg-icons';

library.add(
  faBackspace,
  faBuromobelexperte,
  faCalendarDay,
  faClipboard,
  faClock,
  faCloudDownloadAlt,
  faDove,
  faEgg,
  faGitlab,
  faKiwiBird,
  faFeatherAlt,
  faPencilAlt,
  faPercentage,
  faQuestionCircle,
  faRedo,
  faSave,
  faShareAlt,
  faTh,
  faTimes,
  faTrashAlt,
  faUndo,
  faWindows
);

const Icon = props => {
  return <FontAwesomeIcon {...props} />;
};

export default Icon;
