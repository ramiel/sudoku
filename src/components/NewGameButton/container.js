import { connect } from 'react-redux';
import { createGame, createEmptyEditableGame } from '../../actions/game';
import NewGameButton from './NewGameButton';

const mapDispatchToProps = {
  createGame,
  createEmptyEditableGame,
};

const mapStateToProps = null;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewGameButton);
