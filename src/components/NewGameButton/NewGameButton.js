import React from 'react';
import { InlineBlock, Flex, Button, Popover, Inline } from 'reakit';
import IconButton from '../IconButton';
import Icon from '../Icon';
import './NewGameButton.scss';

const NewGameButton = ({ createGame, createEmptyEditableGame }) => {
  return (
    <Popover.Container>
      {popover => {
        const { hide } = popover;
        return (
          <InlineBlock relative>
            <IconButton use={Popover.Toggle} {...popover} icon="th">
              <Inline>New</Inline>
            </IconButton>
            <Popover
              fade
              slide
              expand
              hideOnClickOutside
              {...popover}
              className="new-game-container"
            >
              <Popover.Arrow />
              <Flex column alignItems="flex-start">
                <Button
                  onClick={() => {
                    createGame({ difficulty: 'easy' });
                    hide();
                  }}
                >
                  <Icon icon="egg" />
                  <span className="difficulty">Easy</span>
                </Button>
                <Button
                  onClick={() => {
                    createGame({ difficulty: 'medium' });
                    hide();
                  }}
                >
                  <Icon icon="kiwi-bird" />
                  <span className="difficulty">Medium</span>
                </Button>
                <Button
                  onClick={() => {
                    createGame({ difficulty: 'hard' });
                    hide();
                  }}
                >
                  <Icon icon="dove" />
                  <span className="difficulty">Hard</span>
                </Button>
                <Button
                  onClick={() => {
                    createEmptyEditableGame();
                    hide();
                  }}
                >
                  <Icon icon="feather-alt" />
                  <span className="difficulty">Custom</span>
                </Button>
              </Flex>
            </Popover>
          </InlineBlock>
        );
      }}
    </Popover.Container>
  );
};

export default NewGameButton;
