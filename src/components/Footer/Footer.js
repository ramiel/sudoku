import React from 'react';
import Icon from '../Icon';
import HelpModal from '../HelpModal';
import './Footer.scss';

const Footer = () => {
  return (
    <div className="footer">
      <span>
        Made for you with ❤ by{' '}
        <a
          href="https://www.ramielcreations.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Ramiel's creations
        </a>
      </span>
      <div className="side">
        <div className="links">
          <HelpModal
            trigger={overlay => {
              return (
                <div className="link" onClick={overlay.show}>
                  Help
                </div>
              );
            }}
          />
          <a
            href="mailto:incoming+ramiel-sudoku-11118659-issue-@incoming.gitlab.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Contact us
          </a>
        </div>
        <a
          href="https://gitlab.com/ramiel/sudoku"
          target="_blank"
          rel="noopener noreferrer"
          aria-label="Source code on GitLab"
        >
          <Icon icon={['fab', 'gitlab']} size="2x" />
        </a>
      </div>
    </div>
  );
};

export default Footer;
