import React, { useState } from 'react';
import QRCode from 'qrcode';

const QrCode = ({ url, width = 200, height = 200 }) => {
  const [qrcode, setQrCode] = useState(null);
  const [error, setError] = useState(null);
  QRCode.toDataURL(url)
    .then(setQrCode)
    .then(() => setError(null))
    .catch(setError);

  return !error ? (
    <img
      width={width}
      height={height}
      src={qrcode}
      alt="qr code for the saved game"
    />
  ) : null;
};

export default QrCode;
