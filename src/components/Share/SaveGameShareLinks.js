import React from 'react';
import { EmailShareButton, EmailIcon } from 'react-share';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Flex } from 'reakit';
import QrCode from './QrCode';
import Icon from '../Icon';
import { serializeSavedGame } from '../../utils';

const SaveGamesShareLinks = ({ game, hide }) => {
  if (!game) return null;

  const sharetext = 'Continue from where I left';
  const { origin, pathname } = document.location;
  const baseUrl = `${origin}${pathname}`;
  const serialized = serializeSavedGame(game);
  const url = `${baseUrl}?savedGame=${encodeURIComponent(serialized)}`;
  const size = 32;
  const round = true;

  return (
    <Flex alignItems="center" column>
      <Flex
        row
        flexWrap="wrap"
        width="140px"
        justifyContent="space-between"
        marginBottom="1rem"
      >
        <EmailShareButton
          subject={sharetext}
          url={url}
          onShareWindowClose={hide}
          className="shareButton"
        >
          <EmailIcon round={round} size={size} />
        </EmailShareButton>
        <CopyToClipboard text={url} onCopy={hide}>
          <span className="shareButton clipboardButton">
            <Icon icon="clipboard" />
          </span>
        </CopyToClipboard>
      </Flex>
      <QrCode url={url} width="400" height="400" />
    </Flex>
  );
};

export default SaveGamesShareLinks;
