import React from 'react';
import {
  FacebookShareButton,
  FacebookIcon,
  // GooglePlusShareButton,
  // LinkedinShareButton,
  TwitterShareButton,
  TwitterIcon,
  // TelegramShareButton,
  // WhatsappShareButton,
  // PinterestShareButton,
  // VKShareButton,
  // OKShareButton,
  // RedditShareButton,
  // TumblrShareButton,
  // LivejournalShareButton,
  // MailruShareButton,
  // ViberShareButton,
  // WorkplaceShareButton,
  // LineShareButton,
  // LineIcon,
  EmailShareButton,
  EmailIcon,
} from 'react-share';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Flex } from 'reakit';
import QrCode from './QrCode';
import Icon from '../Icon';
import { serialize } from '../../utils';

const GameOnlyShareLinks = ({ game, hide = () => {} }) => {
  if (!game) return null;

  const sharetext = 'I just challenged this sudoku, try you too!';
  const { origin, pathname } = document.location;
  const baseUrl = `${origin}${pathname}`;
  const serialized = serialize(game.cells);
  const url = `${baseUrl}?game=${serialized}`;
  const size = 32;
  const round = true;

  return (
    <Flex alignItems="center" column>
      <Flex
        row
        flexWrap="wrap"
        width="140px"
        justifyContent="space-between"
        marginBottom="1rem"
      >
        <FacebookShareButton
          url={url}
          quote={sharetext}
          hashtag="#sudoku"
          onShareWindowClose={hide}
          className="shareButton"
        >
          <FacebookIcon round={round} size={size} />
        </FacebookShareButton>
        <TwitterShareButton
          url={url}
          title={sharetext}
          hashtags={['sudoku']}
          onShareWindowClose={hide}
          className="shareButton"
        >
          <TwitterIcon round={round} size={size} />
        </TwitterShareButton>
        <EmailShareButton
          subject={sharetext}
          url={url}
          onShareWindowClose={hide}
          className="shareButton"
        >
          <EmailIcon round={round} size={size} />
        </EmailShareButton>
        <CopyToClipboard text={url} onCopy={hide}>
          <span className="shareButton clipboardButton">
            <Icon icon="clipboard" />
          </span>
        </CopyToClipboard>
      </Flex>
      <QrCode url={url} />
    </Flex>
  );
};

export default GameOnlyShareLinks;
