import React, { useState } from 'react';
import { InlineBlock, Popover } from 'reakit';
import IconButton from '../IconButton';
import './ShareButton.scss';

const Share = ({ canShare, game }) => {
  const [ShareContent, setShareContent] = useState(null);

  return (
    <Popover.Container>
      {popover => {
        const { hide } = popover;
        return (
          <InlineBlock relative>
            <IconButton
              disabled={!canShare}
              use={Popover.Toggle}
              {...popover}
              width="100%"
              icon="share-alt"
              onClick={async () => {
                if (!ShareContent) {
                  const component = await import('./ShareContent');
                  setShareContent(component);
                }
              }}
            >
              <span>Share</span>
            </IconButton>
            <Popover
              fade
              slide
              expand
              hideOnClickOutside
              {...popover}
              className="new-game-container"
              placement="top-end"
            >
              <Popover.Arrow />
              {ShareContent ? (
                <ShareContent.default hide={hide} game={game} />
              ) : (
                'loading...'
              )}
            </Popover>
          </InlineBlock>
        );
      }}
    </Popover.Container>
  );
};

export default Share;
