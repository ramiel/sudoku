import { connect } from 'react-redux';
import { currentGame } from '../../selectors/game';
import { getSavedGame } from '../../selectors/savings';
import ShareButton from './ShareButton';

const mapDispatchToProps = null;

const mapStateToProps = (state, { id }) => {
  const game = id ? getSavedGame(state, { id }) : currentGame(state);
  return {
    game,
    canShare: !!game,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShareButton);
