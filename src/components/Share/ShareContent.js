import React from 'react';
import { Tabs, Block } from 'reakit';
import GameOnlyShareLinks from './GameOnlyShareLinks';
import SaveGameShareLinks from './SaveGameShareLinks';
import './ShareContent.scss';

export default ({ hide, game }) => {
  return (
    <Tabs.Container>
      {tabs => (
        <Block className="share-content">
          <Tabs className="tabs">
            <Tabs.Tab tab="first" {...tabs}>
              Schema only
            </Tabs.Tab>
            <Tabs.Tab tab="second" {...tabs}>
              Include your moves
            </Tabs.Tab>
          </Tabs>
          <Tabs.Panel tab="first" {...tabs}>
            <p>Share just the game to start a new match from the beginning</p>
            <GameOnlyShareLinks hide={hide} game={game} />
          </Tabs.Panel>
          <Tabs.Panel tab="second" {...tabs}>
            <p>Share every move done until now</p>
            <SaveGameShareLinks hide={hide} game={game} />
          </Tabs.Panel>
        </Block>
      )}
    </Tabs.Container>
  );
};
