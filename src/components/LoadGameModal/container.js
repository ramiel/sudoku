import { connect } from 'react-redux';
import { loadSavedGame } from '../../actions/savings';
import { existSavedGames, getSavedGames } from '../../selectors/savings';
import LoadGameModal from './LoadGameModal';

const mapDispatchToProps = {
  loadSavedGame,
};

const mapStateToProps = state => ({
  existSavedGames: existSavedGames(state),
  savedGames: getSavedGames(state),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoadGameModal);
