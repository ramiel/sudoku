import React, { useState } from 'react';
import { Block, Button, Backdrop, Portal, Overlay, Inline } from 'reakit';
import IconButton from '../IconButton';
import Icon from '../Icon';
import GameResumeModal from '../GameResumeModal';
import './LoadGameModal.scss';

const LoadGameModal = ({
  existSavedGames,
  savedGames,
  loadSavedGame,
  ...others
}) => {
  const [GameList, setGameList] = useState(null);
  return (
    <Overlay.Container>
      {overlay => {
        const { hide } = overlay;
        return (
          <Block>
            <GameResumeModal>
              <IconButton
                disabled={!existSavedGames}
                {...others}
                use={Overlay.Show}
                {...overlay}
                icon="cloud-download-alt"
                onClick={async () => {
                  if (!GameList) {
                    const component = await import('./GameList');
                    setGameList(component);
                  }
                }}
              >
                <Inline>Load</Inline>
              </IconButton>
            </GameResumeModal>
            <Backdrop use={[Portal, Overlay.Hide]} {...overlay} animated fade />
            <Overlay
              use={Portal}
              {...overlay}
              className="load-game-modal-container"
              animated
              fade
            >
              <Block className="content">
                <Button className="close-button" onClick={hide}>
                  <Icon icon="times" />
                </Button>
                <h2>Past games</h2>
                {existSavedGames ? (
                  GameList ? (
                    <GameList.default
                      savedGames={savedGames}
                      loadSavedGame={(...args) => {
                        loadSavedGame(...args);
                        hide();
                      }}
                    />
                  ) : (
                    'loading...'
                  )
                ) : (
                  'No saved games'
                )}
              </Block>
            </Overlay>
          </Block>
        );
      }}
    </Overlay.Container>
  );
};

export default LoadGameModal;
