import React, { useState } from 'react';
import { Flex, Inline, Grid, Block, Divider } from 'reakit';
import IconButton from '../IconButton';
import Icon from '../Icon';
import Share from '../Share';
import DeleteButton from '../DeleteButton';
import { percentageOf } from '../../utils';
import './GameList.scss';

const dateFormat = date => date.toLocaleDateString();
const timeFormat = date =>
  date.toLocaleTimeString({
    hour: 'numeric',
    minute: 'numeric',
    // second: false,
  });

const partialBlock = ({ boardSize, cells }) => {
  const size = Math.sqrt(boardSize);
  const content = [];
  const gridSize = size * size;
  for (let i = 0; i < size; i++) {
    for (let j = 0; j < size; j++) {
      const cellIndex = i * gridSize + j;
      content.push(<Inline key={cellIndex}>{cells[cellIndex].value}</Inline>);
    }
  }
  return (
    <Grid
      templateColumns={`repeat(${size}, 1fr)`}
      templateRows={`repeat(${size}, 1fr)`}
      gap="0"
      className="preview-grid"
      placeItems="center"
    >
      {content}
    </Grid>
  );
};

const difficultyIcon = difficulty => {
  switch (difficulty) {
    case 'easy':
      return <Icon icon="egg" />;
    case 'hard':
      return <Icon icon="dove" />;
    case 'medium':
    default:
      return <Icon icon="kiwi-bird" />;
  }
};

const GameList = ({ savedGames, loadSavedGame }) => {
  const [controlsHidden, hideControl] = useState(null);
  return (
    <Grid
      templateColumns="auto"
      autoRow
      gap="1vw"
      className="game-list-container"
    >
      {savedGames.map(({ game, savedAt }, i) => {
        const filledCells = game.cells.filter(
          ({ value, error, editable }) => !error && value && editable
        ).length;
        const gameCells = game.cells.filter(({ editable }) => !editable).length;
        const total = game.boardSize * game.boardSize;
        const completed = percentageOf(total - gameCells, filledCells);
        return (
          <Flex column key={game.id}>
            <Flex
              row
              alignItems="flex-start"
              justifySelf={i % 2 === 0 ? 'auto' : 'end'}
            >
              <Block className={`preview`}>{partialBlock(game)}</Block>
              <Flex justifyContent="space-between" className={`metadata`}>
                <Flex column>
                  <Inline className="resume">
                    {difficultyIcon(game.difficulty)} {game.difficulty}
                  </Inline>
                  <Inline className="resume">
                    <Icon icon={['fab', 'buromobelexperte']} /> {game.boardSize}
                    x{game.boardSize}
                  </Inline>
                  <Inline className="resume">
                    <Icon icon="percentage" /> {Math.trunc(completed)}% (
                    {filledCells}/{total - gameCells})
                  </Inline>
                </Flex>
                <Flex column>
                  <Inline className="resume">
                    <Icon icon="calendar-day" /> {dateFormat(savedAt)}
                  </Inline>
                  <Inline className="resume">
                    <Icon icon="clock" /> {timeFormat(savedAt)}
                  </Inline>
                </Flex>
              </Flex>
            </Flex>
            <Flex
              row
              marginTop="1rem"
              flexWrap="wrap"
              className="action-buttons"
            >
              {controlsHidden === i ? null : (
                <>
                  <IconButton
                    onClick={() => {
                      loadSavedGame(game.id);
                    }}
                    icon="cloud-download-alt"
                  >
                    <Inline>Load</Inline>
                  </IconButton>
                  <Share id={game.id} />
                </>
              )}
              <DeleteButton
                id={game.id}
                onConfirmShown={shown => hideControl(shown ? i : null)}
              />
            </Flex>
            <Divider />
          </Flex>
        );
      })}
    </Grid>
  );
};

export default GameList;
