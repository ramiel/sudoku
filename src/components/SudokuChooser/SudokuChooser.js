import React, { useEffect, Suspense } from 'react';
import URLSearchParams from '@ungap/url-search-params';
import style from './component.module.scss';

const Sudoku = React.lazy(
  async () => await import(/* webpackChunkName: "Sudoku" */ '../Sudoku')
);
const Footer = React.lazy(
  async () => await import(/* webpackChunkName: "Footer" */ '../Footer')
);

const Loader = () => <div>Wait...</div>;

const SudokuChooser = ({
  currentGame,
  createGame,
  loadSerializedGame,
  loadSerializedSavedGame,
  existSavedGames,
}) => {
  useEffect(() => {
    if (!currentGame) {
      let params = new URLSearchParams(document.location.search.substring(1));
      const serializedGame = params.get('game');
      const savedGame = params.get('savedGame');
      if (savedGame) {
        loadSerializedSavedGame(savedGame);
      } else if (serializedGame) {
        loadSerializedGame(serializedGame);
      } else {
        createGame();
      }
    }
  });

  return (
    <div className={style.sudokuChooser}>
      <Suspense fallback={<Loader />}>
        <div className={style.content}>
          <h1>Sudoku</h1>
          {!!currentGame ? <Sudoku /> : <Loader />}
        </div>
        <Footer />
      </Suspense>
    </div>
  );
};

export default SudokuChooser;
