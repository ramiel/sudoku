import { connect } from 'react-redux';
import {
  createGame,
  loadSerializedGame,
  loadSerializedSavedGame,
} from '../../actions/game';
import { getCurrentGameId } from '../../selectors/game';
import { existSavedGames } from '../../selectors/savings';
import SudokuChooser from './SudokuChooser';

const mapDispatchToProps = {
  createGame,
  loadSerializedGame,
  loadSerializedSavedGame,
};

const mapStateToProps = state => ({
  currentGame: getCurrentGameId(state),
  existSavedGames: existSavedGames(state),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SudokuChooser);
