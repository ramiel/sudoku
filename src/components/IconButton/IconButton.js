import React from 'react';
import Button from '../Button';
import Icon from '../Icon';
import { Flex } from 'reakit';

const IconButton = ({ children, icon, use = [], ...props }) => {
  const otherUse = Array.isArray(use) ? use : [use];
  return (
    <Button
      justifyContent="space-around"
      alignItems="center"
      {...props}
      use={[Flex, ...otherUse]}
    >
      <Icon icon={icon} style={{ marginRight: '0.5rem' }} />
      {children}
    </Button>
  );
};

export default IconButton;
