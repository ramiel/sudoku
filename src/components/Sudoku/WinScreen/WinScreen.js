import React, { useState } from 'react';
import { Block, Backdrop, Portal, Overlay, Button } from 'reakit';
import GameOnlyShareLinks from '../../Share/GameOnlyShareLinks';
import Icon from '../../Icon';
import './WinScreen.scss';

export default ({
  // from conatiner
  hasWin,
  elapsed,
  game,
}) => {
  const [visible, setVisible] = useState(false);
  let [prevVisible, setPrevVisible] = useState(null);

  if (hasWin !== prevVisible) {
    setVisible(hasWin);
    setPrevVisible(hasWin);
  }
  const hide = () => setVisible(false);

  return (
    <Block>
      <Backdrop use={[Portal]} visible={visible} />
      <Overlay
        use={Portal}
        visible={visible}
        hide={hide}
        hideOnEsc
        hideOnClickOutside
      >
        <Block className="win-screen">
          <Button className="close-button" onClick={hide}>
            <Icon icon="times" />
          </Button>
          <h2 className="title">Hurray!</h2>
          <p>Congratulation, you solved this sudoku.</p>
          <p>Share it with your friends and challenge them!</p>
          <GameOnlyShareLinks game={game} />
        </Block>
      </Overlay>
    </Block>
  );
};
