import { connect } from 'react-redux';
import { hasWin, elapsed, currentGame } from '../../../selectors/game';
import WinScreen from './WinScreen';

const mapDispatchToProps = null;

const mapStateToProps = (state, props) => ({
  hasWin: hasWin(state),
  elapsed: elapsed(state),
  game: currentGame(state),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WinScreen);
