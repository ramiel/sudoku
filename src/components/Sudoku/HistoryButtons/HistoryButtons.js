import React, { useEffect } from 'react';
import Button from '../../Button';
import Icon from '../../Icon';
import { Flex } from 'reakit';

const onKeyUp = ({ clearCurrentCell, setCurrentCellValue, undo, redo }) => {
  return e => {
    if (e.ctrlKey) {
      if (e.key === 'Z') {
        return redo();
      }
      if (e.key === 'z') {
        return undo();
      } else if (e.key === 'y') {
        return redo();
      }
    }
  };
};

export const UndoButton = ({ undo, hasPastHistory }) => {
  return (
    <Button onClick={undo} disabled={!hasPastHistory} title="CTRL+z">
      <Icon icon="undo" />
    </Button>
  );
};

export const RedoButton = ({ redo, hasFutureHistory, ...others }) => {
  return (
    <Button onClick={redo} disabled={!hasFutureHistory} title="CTRL+y">
      <Icon icon="redo" />
    </Button>
  );
};

export default ({ undo, redo, hasPastHistory, hasFutureHistory }) => {
  // Hooks
  useEffect(() => {
    const fnKeyUp = onKeyUp({ undo, redo });
    window.addEventListener('keyup', fnKeyUp);
    return () => {
      window.removeEventListener('keyup', fnKeyUp);
    };
  });

  return (
    <Flex justifyContent="space-between">
      <UndoButton undo={undo} hasPastHistory={hasPastHistory} />
      <RedoButton redo={redo} hasFutureHistory={hasFutureHistory} />
    </Flex>
  );
};
