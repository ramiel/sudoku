import { connect } from 'react-redux';
import { ActionCreators } from 'redux-undo';
import { hasPastHistory, hasFutureHistory } from '../../../selectors/game';
import HistoryButtons from './HistoryButtons';

const mapDispatchToProps = {
  undo: ActionCreators.undo,
  redo: ActionCreators.redo,
};

const mapStateToProps = state => ({
  hasPastHistory: hasPastHistory(state),
  hasFutureHistory: hasFutureHistory(state),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HistoryButtons);
