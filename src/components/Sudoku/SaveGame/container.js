import { connect } from 'react-redux';
import { saveCurrentGame } from '../../../actions/savings';
import { currentGameHasOneUserMove } from '../../../selectors/game';
import { canBeSavedCurrentGame } from '../../../selectors/savings';
import SaveGame from './SaveGame';

const mapDispatchToProps = { saveCurrentGame };

const mapStateToProps = state => ({
  disabled: !canBeSavedCurrentGame(state),
  oneMove: currentGameHasOneUserMove(state),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SaveGame);
