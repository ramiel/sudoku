import React, { useEffect } from 'react';
import IconButton from '../../IconButton';

const SaveGame = ({ saveCurrentGame, game, disabled, oneMove, ...others }) => {
  useEffect(() => {
    const onBeforeUnload = event => {
      if (disabled || !oneMove) {
        return;
      }
      event.preventDefault();
      const message =
        'This game has not been saved. Are you sure you want to leave?';
      event.returnValue = message;
      return message;
    };
    window.addEventListener('beforeunload', onBeforeUnload);
    return () => {
      window.removeEventListener('beforeunload', onBeforeUnload);
    };
  });
  return (
    <IconButton
      disabled={disabled}
      onClick={saveCurrentGame}
      {...others}
      width="100%"
      icon="save"
    >
      <span>Save</span>
    </IconButton>
  );
};

export default React.memo(SaveGame, (prevProps, nextProps) => {
  return (
    prevProps.disabled === nextProps.disabled &&
    prevProps.oneMove === nextProps.oneMove
  );
});
