import React from 'react';
import { Grid, Inline } from 'reakit';

export default ({ index, size, guesses }) => {
  const content = [];
  const sideSize = Math.sqrt(size);
  for (let i = 1; i <= size; i++) {
    content.push(
      <Inline key={`guess-${index}-${i}`}>
        {guesses.indexOf(i) !== -1 ? i : null}
      </Inline>
    );
  }

  return (
    <Grid
      templateColumns={`repeat(${sideSize}, 1fr)`}
      templateRows={`repeat(${sideSize}, 1fr)`}
      gap="0"
      placeItems="center"
      width="100%"
      height="100%"
      fontSize="0.5em"
      lineHeight="1em"
    >
      {content}
    </Grid>
  );
};
