import { connect } from 'react-redux';
import { getCurrentGameBoardSize, cellGuesses } from '../../../selectors/game';
import GuessGrid from './GuessGrid';

const mapDispatchToProps = null;

const mapStateToProps = (state, { index }) => ({
  size: getCurrentGameBoardSize(state),
  guesses: cellGuesses(state, { index }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GuessGrid);
