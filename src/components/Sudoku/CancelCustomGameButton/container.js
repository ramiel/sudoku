import { connect } from 'react-redux';
import { deleteEditableGame } from '../../../actions/game';
import CancelCustomGameButton from './CancelCustomGameButton';

const mapDispatchToProps = { deleteEditableGame };

const mapStateToProps = state => ({
  disabled: false,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CancelCustomGameButton);
