import React from 'react';
import IconButton from '../../IconButton';

const CancelCustomGameButton = ({
  deleteEditableGame,
  disabled,
  ...others
}) => {
  return (
    <IconButton
      disabled={disabled}
      onClick={deleteEditableGame}
      {...others}
      icon="times"
    >
      <span>Cancel</span>
    </IconButton>
  );
};

export default React.memo(CancelCustomGameButton);
