import React from 'react';
import { Flex, Inline } from 'reakit';
import HystoryButtons from './HistoryButtons';
import WinScreen from './WinScreen';
import MobileActionSelector from './MobileActionSelector';
import SaveGame from './SaveGame';
import NewGameButton from '../NewGameButton';
import ShareGameButton from '../Share';
import HelpModal from '../HelpModal';
import LoadGameModal from '../LoadGameModal';
import Icon from '../Icon';
import SudokuTable from '../SudokuTable';
import SaveCustomGameButton from './SaveCustomGameButton';
import CancelCustomGameButton from './CancelCustomGameButton';
import './Sudoku.scss';

const DifficultyIcon = ({ difficulty }) => {
  switch (difficulty) {
    case 'easy':
      return <Icon icon="egg" />;
    case 'hard':
      return <Icon icon="dove" />;
    case 'custom':
      return <Icon icon="feather-alt" />;
    case 'medium':
    default:
      return <Icon icon="kiwi-bird" />;
  }
};

export default ({
  // from container
  size,
  difficulty,
  editable,
}) => {
  return (
    <Flex column className="main-content">
      <Flex className="main-title">
        <Flex>
          <Inline marginRight="0.5em">
            <DifficultyIcon difficulty={difficulty} />
          </Inline>
          <Inline>{difficulty}</Inline>
          <HelpModal
            trigger={({ show }) => {
              return (
                <Inline onClick={show} cursor="pointer" marginLeft="0.5em">
                  <Icon icon="question-circle" />
                </Inline>
              );
            }}
          />
        </Flex>
        <Flex className="top-buttons">
          <LoadGameModal marginRight="0.5em" />
          <NewGameButton />
        </Flex>
      </Flex>

      <Flex row className="game-container" role="grid">
        <SudokuTable size={size} />
        <Flex justifyContent="flex-end" column>
          <Flex
            justifyContent="space-between"
            alignItems="flex-start"
            className="controls"
          >
            <MobileActionSelector />
            <Flex column justifyContent="space-between" minHeight="150px">
              {editable ? (
                <Flex column>
                  <SaveCustomGameButton marginBottom="1em" />
                  <CancelCustomGameButton />
                </Flex>
              ) : (
                <>
                  <ShareGameButton />
                  <SaveGame />
                  <HystoryButtons />
                </>
              )}
            </Flex>
          </Flex>

          {editable ? (
            <Flex maxWidth="300px" column alignItems="center">
              <h2>Build your own sudoku</h2>
              <p className="build-instructions">
                Insert at least <strong>17</strong> digits and play your own
                sudoku board.
                <br />
                When you finish, hit the "Save and Play" button; your game will
                be automatically saved to your library and you can share it with
                your friends!
              </p>
            </Flex>
          ) : null}
        </Flex>
      </Flex>
      <WinScreen />
    </Flex>
  );
};
