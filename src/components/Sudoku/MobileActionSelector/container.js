import { connect } from 'react-redux';
import {
  clearCurrentCell,
  setCurrentCellValue,
  toggleCurrentCellGuess,
} from '../../../actions/game';
import {
  getCurrentGameBoardSize,
  isCurrentCellEditable,
  isCurrentGameEditable,
} from '../../../selectors/game';
import MobileActionSelector from './MobileActionSelector';

const mapDispatchToProps = {
  clearCurrentCell,
  setCurrentCellValue,
  toggleCurrentCellGuess,
};

const mapStateToProps = (state, { index }) => ({
  disableGuess: isCurrentGameEditable(state),
  size: getCurrentGameBoardSize(state),
  disabled: !isCurrentCellEditable(state),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MobileActionSelector);
