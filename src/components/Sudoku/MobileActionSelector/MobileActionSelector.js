import React, { useState, useEffect } from 'react';
import { Flex, Inline, Button, Grid } from 'reakit';
import Icon from '../../Icon';
import './MobileActionSelector.scss';

const MobileActionSelector = ({
  clearCurrentCell,
  setCurrentCellValue,
  toggleCurrentCellGuess,
  disabled,
  disableGuess,
  size,
  className = '',
}) => {
  const [guessActive, setGuessActive] = useState(false);
  const onKeyDown = e => {
    if (!disableGuess && e.key === 'Meta') {
      return setGuessActive(true);
    }
  };
  const onKeyUp = e => {
    if (e.key === 'Meta') {
      return setGuessActive(false);
    }
  };
  useEffect(() => {
    window.addEventListener('keydown', onKeyDown);
    window.addEventListener('keyup', onKeyUp);
    return () => {
      window.removeEventListener('keydown', onKeyDown);
      window.removeEventListener('keyup', onKeyUp);
    };
  });

  const buttons = [];
  for (let i = 1; i <= size; i++) {
    buttons.push(
      <Button
        className={`keyboard-btn ${guessActive ? 'guess' : ''}`}
        disabled={disabled}
        onClick={() =>
          guessActive ? toggleCurrentCellGuess(i) : setCurrentCellValue(i)
        }
        key={`button-${i}`}
      >
        {i}
      </Button>
    );
  }

  return (
    <>
      <Grid
        templateColumns="repeat(3, 1fr)"
        autoRows="auto"
        gap="1vw"
        width="100%"
      >
        {buttons}
        <Button
          className="keyboard-btn"
          disabled={disabled}
          onClick={clearCurrentCell}
          aria-label="Clear cell"
        >
          <Icon icon="backspace" />
        </Button>
        <Grid.Item gridColumnEnd="span 2">
          <Button
            className="keyboard-btn guess-switch"
            onClick={() => setGuessActive(!guessActive)}
            disabled={disableGuess}
          >
            <Flex alignItems="center">
              <Icon
                icon="pencil-alt"
                transform={guessActive ? '' : 'down-6 rotate-45'}
                style={{ opacity: guessActive ? 1 : 0.5 }}
              />
              <Inline marginLeft="0.5rem">
                {guessActive ? 'Guess  ON ' : 'Guess OFF'}
              </Inline>
            </Flex>
          </Button>
        </Grid.Item>
      </Grid>
    </>
  );
};

export default MobileActionSelector;
