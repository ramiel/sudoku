import { connect } from 'react-redux';
import {
  getCurrentGameBoardSize,
  getCurrentGameDifficulty,
  isCurrentGameEditable,
} from '../../selectors/game';
import Sudoku from './Sudoku';

const mapDispatchToProps = null;

const mapStateToProps = (state, props) => ({
  size: getCurrentGameBoardSize(state),
  difficulty: getCurrentGameDifficulty(state),
  editable: isCurrentGameEditable(state),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sudoku);
