import React from 'react';
import { Flex } from 'reakit';
import GuessGrid from '../GuessGrid';
import './Cell.scss';

export default ({
  index,
  // from container
  value,
  isGuessing,
  editable,
  selected,
  select,
  isPositionallyRelated,
  isNumericallyRelated,
  hasError,
}) => {
  return (
    <Flex
      className={`cell 
        ${editable ? 'editable' : ''} 
        ${selected ? 'selected' : ''} 
        ${isPositionallyRelated ? 'highlightPos' : ''} 
        ${isNumericallyRelated ? 'highlightNum' : ''} 
        ${hasError ? 'error' : ''}`}
      onClick={select}
      role="cell"
      justifyContent="center"
      alignItems="center"
    >
      {isGuessing ? <GuessGrid index={index} /> : value}
    </Flex>
  );
};
