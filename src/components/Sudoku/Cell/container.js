import { connect } from 'react-redux';
import { selectCell } from '../../../actions/game';
import {
  cellValue,
  cellIsGuessing,
  cellIsEditable,
  cellIsSelected,
  cellIsPositionallyRelated,
  cellIsNumericallyRelated,
  cellHasError,
} from '../../../selectors/game';
import Cell from './Cell';

const mapDispatchToProps = (dispatch, { index }) => ({
  select: () => dispatch(selectCell(index)),
});

const mapStateToProps = (state, { index }) => ({
  value: cellValue(state, { index }),
  isGuessing: cellIsGuessing(state, { index }),
  editable: cellIsEditable(state, { index }),
  selected: cellIsSelected(state, { index }),
  isPositionallyRelated: cellIsPositionallyRelated(state, { index }),
  isNumericallyRelated: cellIsNumericallyRelated(state, { index }),
  hasError: cellHasError(state, { index }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cell);
