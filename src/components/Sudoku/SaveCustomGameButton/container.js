import { connect } from 'react-redux';
import { saveEditableGame } from '../../../actions/game';
import { gameHasValidClues } from '../../../selectors/game';
import SaveCustomGameButton from './SaveCustomGameButton';

const mapDispatchToProps = { saveEditableGame };

const mapStateToProps = state => ({
  disabled: !gameHasValidClues(state),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SaveCustomGameButton);
