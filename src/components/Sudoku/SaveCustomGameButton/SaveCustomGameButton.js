import React from 'react';
import IconButton from '../../IconButton';

const SaveCustomGameButton = ({ saveEditableGame, disabled, ...others }) => {
  return (
    <IconButton
      disabled={disabled}
      onClick={() => saveEditableGame()}
      {...others}
      icon="save"
    >
      <span>Save & play</span>
    </IconButton>
  );
};

export default React.memo(SaveCustomGameButton);
