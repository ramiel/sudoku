import { connect } from 'react-redux';
import { deleteGame } from '../../actions/savings';
import DeleteButton from './DeleteButton';

const mapDispatchToProps = (dispatch, { id }) => ({
  deleteGame: () => dispatch(deleteGame(id)),
});

const mapStateToProps = null;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteButton);
