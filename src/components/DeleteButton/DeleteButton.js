import React, { useState } from 'react';
import { Inline, Flex, Button } from 'reakit';
import IconButton from '../IconButton';
import './DeleteButton.scss';

const DeleteButton = ({
  id,
  deleteGame,
  onConfirmShown = () => {},
  ...props
}) => {
  const [confirmShown, showConfirm] = useState(false);
  return confirmShown ? (
    <Flex row {...props}>
      <Button
        marginRight="1rem"
        onClick={() => {
          showConfirm(false);
          onConfirmShown(false);
        }}
        className="cancel-btn"
      >
        Cancel
      </Button>
      <IconButton
        onClick={() => {
          deleteGame();
          onConfirmShown(false);
        }}
        {...props}
        icon="trash-alt"
        className="delete-btn"
      >
        <Inline>Confirm</Inline>
      </IconButton>
    </Flex>
  ) : (
    <IconButton
      onClick={() => {
        showConfirm(true);
        onConfirmShown(true);
      }}
      {...props}
      icon="trash-alt"
      className="delete-btn"
    >
      <Inline>Delete</Inline>
    </IconButton>
  );
};
export default DeleteButton;
