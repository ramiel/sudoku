import React from 'react';
import { Button } from 'reakit';
import './Button.scss';

export default ({ children, className = '', ...props }) => (
  <Button className={`button-generic ${className}`} {...props}>
    {children}
  </Button>
);
