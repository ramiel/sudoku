import { persistReducer, createTransform } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const initialState = {
  // example game
  // [id]: {...game}
};

const reducers = {
  SAVE_GAME: (state, { game }) => {
    return {
      ...state,
      [game.id]: {
        savedAt: new Date(),
        game,
      },
    };
  },

  DELETE_GAME: (state, { id }) => {
    const newState = { ...state };
    delete newState[id];
    return newState;
  },
};

const reducer = (state = initialState, action) => {
  if (reducers[action.type]) {
    return reducers[action.type](state, action);
  }
  return state;
};

const persistenceConfig = {
  key: 'savings',
  storage: storage,
  transforms: [
    createTransform(null, (outboundState /* , key */) => {
      if (outboundState.game) {
        const { game } = outboundState;
        return {
          ...outboundState,
          savedAt: new Date(outboundState.savedAt),
          game: {
            ...game,
            startedAt: game.startedAt && new Date(game.startedAt),
            updatedAt: game.updatedAt && new Date(game.updatedAt),
            finishedAt: game.finishedAt && new Date(game.finishedAt),
          },
        };
      }
      return outboundState;
    }),
  ],
};

export default persistReducer(persistenceConfig, reducer);
