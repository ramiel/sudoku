const initialState = {
  currentGame: null,
};

const reducers = {
  CREATE_GAME: (state, { id }) => {
    return {
      ...state,
      currentGame: id,
    };
  },

  LOAD_GAME: (state, {game: {id}}) => {
    return {
      ...state,
      currentGame: id,
    };
  }
};

export default (state = initialState, action) => {
  if (reducers[action.type]) {
    return reducers[action.type](state, action);
  }
  return state;
};
