import game from './game';
import savings from './savings';
import user from './user';

export default {
  game,
  savings,
  user,
};
