import undoable, { includeAction } from 'redux-undo';
import { hasCellError, cellCountToDifficulty } from '../utils';
import undoExtra from './hor/undoExtra';

/**
 * Cell content
 * {
 *   value: Number,
 *   guesses: [Number],
 *   error: Bool
 * }
 */

const initialState = {
  id: null,
  difficulty: null,
  boardSize: 0,
  currentCell: 0,
  currentRow: 0,
  currentColumn: 0,
  currentBlock: 0,
  cells: null,
  filledCells: 0,
  startedAt: null,
  updatedAt: null,
  finishedAt: null,
  editable: false,
};

const reducers = {
  SELECT_CELL: (state, { cell }) => {
    const { boardSize } = state;
    const blockSideSize = Math.sqrt(boardSize);
    const currentRow = Math.floor(cell / boardSize);
    const currentColumn = cell % boardSize;
    const currentBlock =
      Math.floor(currentColumn / blockSideSize) +
      Math.floor(currentRow / blockSideSize) * blockSideSize;
    return {
      ...state,
      currentCell: cell,
      currentRow,
      currentColumn,
      currentBlock,
    };
  },

  MOVE: (state, { direction }) => {
    const { currentCell, boardSize } = state;
    switch (direction.toLowerCase()) {
      case 'up': {
        const nextCell =
          currentCell < boardSize ? currentCell : currentCell - boardSize;
        return reducers.SELECT_CELL(state, { cell: nextCell });
      }
      case 'down': {
        const nextCell =
          currentCell > boardSize * boardSize - boardSize - 1
            ? currentCell
            : currentCell + boardSize;
        return reducers.SELECT_CELL(state, { cell: nextCell });
      }
      case 'right': {
        const nextCell = Math.min(boardSize * boardSize - 1, currentCell + 1);
        return reducers.SELECT_CELL(state, { cell: nextCell });
      }
      case 'left': {
        const nextCell = Math.max(0, currentCell - 1);
        return reducers.SELECT_CELL(state, { cell: nextCell });
      }
      default:
        return state;
    }
  },

  CLEAR_CURRENT_CELL: state => {
    if (!state.cells[state.currentCell].editable) {
      return state;
    }
    const isEmpty = !state.cells[state.currentCell].value;
    const filledCells = isEmpty ? state.filledCells : state.filledCells - 1;
    return {
      ...state,
      filledCells,
      updatedAt: new Date(),
      cells: [
        ...state.cells.slice(0, state.currentCell),
        {
          ...state.cells[state.currentCell],
          value: null,
          error: false,
          guesses: [],
        },
        ...state.cells.slice(state.currentCell + 1),
      ],
    };
  },

  SET_CURRENT_CELL_VALUE: (state, { value }) => {
    if (!state.cells[state.currentCell].editable) {
      return state;
    }
    const isEmpty = !state.cells[state.currentCell].value;
    const filledCells = isEmpty ? state.filledCells + 1 : state.filledCells;
    const newCell = {
      ...state.cells[state.currentCell],
      value,
      guesses: [],
    };
    newCell.error = hasCellError(state.cells, newCell);
    return {
      ...state,
      filledCells,
      updatedAt: new Date(),
      cells: [
        ...state.cells.slice(0, state.currentCell),
        newCell,
        ...state.cells.slice(state.currentCell + 1),
      ],
    };
  },

  /**
   * Given a guess add it to the cell or remove it if already present
   */
  TOGGLE_CURRENT_CELL_GUESS: (state, { guess }) => {
    if (!state.cells[state.currentCell].editable) {
      return state;
    }
    const { guesses = [] } = state.cells[state.currentCell];
    const indexOfGuess = guesses.indexOf(guess);
    const newGuesses =
      indexOfGuess === -1
        ? [...guesses, guess]
        : [
            ...guesses.slice(0, indexOfGuess),
            ...guesses.slice(indexOfGuess + 1),
          ];
    return {
      ...state,
      ...reducers.CLEAR_CURRENT_CELL(state),
      cells: [
        ...state.cells.slice(0, state.currentCell),
        {
          ...state.cells[state.currentCell],
          value: null,
          error: false,
          guesses: newGuesses,
        },
        ...state.cells.slice(state.currentCell + 1),
      ],
    };
  },

  CREATE_GAME: (state, { id, size, difficulty, cells, editable = false }) => {
    const now = new Date();
    return {
      ...state,
      id,
      boardSize: size,
      difficulty,
      cells,
      filledCells: cells.filter(({ value }) => !!value).length,
      startedAt: editable ? null : now,
      updatedAt: editable ? null : now,
      editable,
    };
  },

  SAVE_EDITABLE_GAME: state => {
    const now = new Date();
    const filledCells = state.cells.filter(({ value }) => !!value).length;
    return {
      ...state,
      cells: state.cells.map(cell => {
        return !!cell.value
          ? {
              ...cell,
              editable: false,
            }
          : cell;
      }),
      difficulty: cellCountToDifficulty(filledCells),
      filledCells,
      startedAt: now,
      updatedAt: now,
      editable: false,
    };
  },

  WIN: state => {
    return {
      ...state,
      finishedAt: state.finishedAt || new Date(),
    };
  },

  LOAD_GAME: (state, { game }) => game,
};

const reducer = (state = initialState, action) => {
  if (reducers[action.type]) {
    return reducers[action.type](state, action);
  }
  return state;
};

export default undoExtra(
  undoable(reducer, {
    filter: includeAction([
      'CLEAR_CURRENT_CELL',
      'SET_CURRENT_CELL_VALUE',
      'TOGGLE_CURRENT_CELL_GUESS',
      'CREATE_GAME',
      'LOAD_GAME',
    ]),
    initTypes: ['@@redux-undo/INIT'],
    ignoreInitialState: true,
  })
);
