/**
 * A series of extra feature for the redux-undo plugin
 */

const CLEAN_FUTURE = '@@redux-undo-extra/CLEAN_FUTURE';

export const UndoExtraActionCreators = {
  cleanFuture: () => ({
    type: CLEAN_FUTURE,
  }),
};

const undoExtra = reducer => (state, action) => {
  if (action.type === CLEAN_FUTURE) {
    return {
      ...state,
      future: [],
    };
  }
  return reducer(state, action);
};

export default undoExtra;
