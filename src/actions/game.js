import {
  generateCells,
  generateEmptyCells,
  deserialize,
  createId,
  deserializeSavedGame,
} from '../utils';
import { getPreviousGameHistoryIndex } from '../selectors/game';
import { saveCurrentGame } from './savings';
import { UndoExtraActionCreators } from '../reducers/hor/undoExtra';
import { ActionCreators } from 'redux-undo';

export const selectCell = cell => ({
  type: 'SELECT_CELL',
  cell,
});

export const moveCell = direction => ({
  type: 'MOVE',
  direction,
});

export const moveCellUp = () => moveCell('up');
export const moveCellDown = () => moveCell('down');
export const moveCellRight = () => moveCell('right');
export const moveCellLeft = () => moveCell('left');

export const clearCurrentCell = () => ({
  type: 'CLEAR_CURRENT_CELL',
});

export const setCurrentCellValue = value => ({
  type: 'SET_CURRENT_CELL_VALUE',
  value,
});

export const toggleCurrentCellGuess = guess => ({
  type: 'TOGGLE_CURRENT_CELL_GUESS',
  guess,
});

export const createGame = ({ size = 9, difficulty = 'easy' } = {}) => {
  const cells = generateCells({ size, difficulty });
  return {
    type: 'CREATE_GAME',
    id: createId(),
    size,
    difficulty,
    cells,
  };
};

export const createEmptyEditableGame = ({ size = 9 } = {}) => {
  const cells = generateEmptyCells({ size });
  return {
    type: 'CREATE_GAME',
    id: createId(),
    size,
    cells,
    editable: true,
    difficulty: 'custom',
  };
};

export const saveEditableGame = () => (dispatch, getState) => {
  dispatch({
    type: 'SAVE_EDITABLE_GAME',
  });
  dispatch(saveCurrentGame());
};

export const deleteEditableGame = () => (dispatch, getState) => {
  const state = getState();
  const pastIndex = getPreviousGameHistoryIndex(state);
  if (pastIndex === -1) {
    dispatch(createGame());
  } else {
    dispatch(ActionCreators.jumpToPast(pastIndex));
    dispatch(UndoExtraActionCreators.cleanFuture());
  }
};

export const loadSerializedGame = serializedGame => {
  const { cells, difficulty } = deserialize(serializedGame);
  const size = Math.sqrt(cells.length);
  if (!Number.isInteger(size)) {
    throw new Error('Invalid game');
  }
  return {
    type: 'CREATE_GAME',
    id: createId(),
    size,
    difficulty,
    cells,
  };
};

export const loadSerializedSavedGame = serializedSavedGame => {
  const game = deserializeSavedGame(serializedSavedGame);
  return {
    type: 'LOAD_GAME',
    game,
  };
};
