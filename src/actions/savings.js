import { ActionCreators } from 'redux-undo';
import { getSavedGame, getLatestSavedGame } from '../selectors/savings';
import { currentGame } from '../selectors/game';

export const saveGame = game => {
  console.warn('saveGame is deprecated. Use saveCurrentGame instead');
  return {
    type: 'SAVE_GAME',
    game,
  };
};

export const saveCurrentGame = () => (dispatch, getState) => {
  const state = getState();
  const game = currentGame(state);
  return dispatch({
    type: 'SAVE_GAME',
    game,
  });
};

export const loadSavedGame = id => (dispatch, getState) => {
  const game = getSavedGame(getState(), { id });
  dispatch(ActionCreators.clearHistory());
  dispatch({
    type: 'LOAD_GAME',
    game,
  });
};

export const loadLastSavedGame = () => (dispatch, getState) => {
  const game = getLatestSavedGame(getState());
  dispatch(ActionCreators.clearHistory());
  dispatch({
    type: 'LOAD_GAME',
    game,
  });
};

export const deleteGame = id => ({
  type: 'DELETE_GAME',
  id,
});
